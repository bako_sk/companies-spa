import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {CompaniesModule} from './companies/companies.module';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {DivisionsModule} from './divisions/divisions.module';
import {ProjectsModule} from './projects/projects.module';
import {DepartmentsModule} from './departments/departments.module';
import {EmployeesModule} from './employees/employees.module';
import {HttpErrorInterceptor} from './shared/HttpErrorInterceptor';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    CompaniesModule,
    DivisionsModule,
    ProjectsModule,
    DepartmentsModule,
    EmployeesModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true}
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
