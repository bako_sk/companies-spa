import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './shared/not-found/not-found.component';
import {ServerErrorComponent} from './shared/server-error/server-error.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/companies', pathMatch: 'full'},
  {path: 'divisions', redirectTo: '/companies', pathMatch: 'full'},
  {path: 'projects', redirectTo: '/companies', pathMatch: 'full'},
  {path: 'departments', redirectTo: '/companies', pathMatch: 'full'},
  {path: 'employees', redirectTo: '/companies', pathMatch: 'full'},
  {path: 'not-found', component: NotFoundComponent},
  {path: 'server-error', component: ServerErrorComponent},

];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}

