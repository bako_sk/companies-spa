import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectDetailComponent} from './project-detail/project-detail.component';

const projectRoutes: Routes = [
  {path: 'projects/:id', component: ProjectDetailComponent}
];
@NgModule({
  imports: [
    RouterModule.forChild(projectRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProjectRoutingModule {}
