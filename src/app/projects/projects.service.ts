import { Injectable } from '@angular/core';
import {ProjectModel} from '../models/project-model';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private currentProject: ProjectModel;
  public currentProjectChanged = new Subject<ProjectModel>();

  constructor(private http: HttpClient) { }

  public getCurrentProject() : ProjectModel
  {
    return this.currentProject;
  }

  public setCurrentProject(project: ProjectModel): void
  {
    this.currentProject = project;
    this.currentProjectChanged.next(project);
  }

  public fetchProject(id: number): void
  {
    this.http.get<ProjectModel>(environment.apiUrl + 'projects/' + id)
      .pipe(
        map((project: ProjectModel) =>
        {
          console.log(project);
          return {
            ...project,
            departments: project.departments ? project.departments : [],
            headOfProject: project.head ? project.head : null
          }
        }),
        tap((project: ProjectModel) =>
        {
          this.setCurrentProject(project);
        })
      )
      .subscribe();
  }
}
