import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProjectsService} from '../projects.service';
import {ActivatedRoute, Params} from '@angular/router';
import {ProjectModel} from '../../models/project-model';
import {AddEmployeeActions} from '../../employees/add-employee-actions';
import {EmployeesService} from '../../employees/employees.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

  public addType = AddEmployeeActions.ADD_PROJECT_HEAD;
  public project: ProjectModel;

  private currentProjectChangedSubscription: Subscription;
  private employeeRemovedSubscription: Subscription;

  constructor(
    private projectService: ProjectsService,
    private activatedRoute: ActivatedRoute,
    private employeesService: EmployeesService
  ) { }

  ngOnInit() {
    this.project = this.projectService.getCurrentProject();
    this.activatedRoute.params.subscribe((params: Params) =>
    {
      this.projectService.fetchProject(+params['id']);
    });
    this.currentProjectChangedSubscription = this.projectService.currentProjectChanged
      .subscribe((project: ProjectModel) =>
      {
        this.project = project;
      }
    );
    this.employeeRemovedSubscription = this.employeesService.employeeRemoved
      .subscribe((removedId: number) =>
      {
        if(this.project && this.project.head.employeeID == removedId)
        {
          this.project.head = null;
        }
      });
  }

  ngOnDestroy(): void
  {
    this.currentProjectChangedSubscription.unsubscribe();
    this.employeeRemovedSubscription.unsubscribe();
  }

}
