import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import {ProjectRoutingModule} from './project-routing.module';
import {SharedModule} from '../shared/shared.module';
import {EmployeesModule} from '../employees/employees.module';



@NgModule({
  declarations: [ProjectDetailComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    SharedModule,
    EmployeesModule
  ]
})
export class ProjectsModule { }
