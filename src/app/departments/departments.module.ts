import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentDetailComponent } from './department-detail/department-detail.component';
import {DepartmentsRoutingModule} from './departments-routing.module';
import {EmployeesModule} from '../employees/employees.module';



@NgModule({
  declarations: [DepartmentDetailComponent],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    EmployeesModule
  ]
})
export class DepartmentsModule { }
