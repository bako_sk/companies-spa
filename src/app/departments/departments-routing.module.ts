import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DepartmentDetailComponent} from './department-detail/department-detail.component';

const departmentRoutes: Routes = [
  {path: 'departments/:id', component: DepartmentDetailComponent}
];
@NgModule({
  imports: [
    RouterModule.forChild(departmentRoutes),
  ],
  exports: [
    RouterModule
  ]
})
export class DepartmentsRoutingModule {}
