import {Component, OnDestroy, OnInit} from '@angular/core';
import {DepartmentModel} from '../../models/department.model';
import {Subscription} from 'rxjs';
import {DepartmentsService} from '../departments.service';
import {ActivatedRoute, Params} from '@angular/router';
import {EmployeesService} from '../../employees/employees.service';
import {AddEmployeeActions} from '../../employees/add-employee-actions';

@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.css']
})
export class DepartmentDetailComponent implements OnInit,OnDestroy {

  public addTypeDepEmployee = AddEmployeeActions.ADD_DEPARTMENT_EMPLOYEE;
  public addTypeHead = AddEmployeeActions.ADD_DEPARTMENT_HEAD;
  public editing: boolean;
  public department: DepartmentModel;

  private  currentDepartmentChangedSubscription: Subscription;
  private editingChangedSubscription: Subscription;
  private employeeRemovedSubscription: Subscription;

  constructor(
    private departmentsService: DepartmentsService,
    private activatedRoute: ActivatedRoute,
    private employeesService: EmployeesService
  ) { }

  ngOnInit()
  {
    this.department = this.departmentsService.getCurrentDepartment();
    this.activatedRoute.params.subscribe((params: Params) =>
    {
      this.departmentsService.fetchDepartment(+params['id']);
    });

    this.currentDepartmentChangedSubscription = this.departmentsService.currentDepartmentChanged.subscribe(
      (department: DepartmentModel) =>
      {
        this.department = department;
      }
    );
    this.editing = false;
    this.editingChangedSubscription  = this.employeesService.editingChanged
      .subscribe((editing: boolean) =>
      {
        this.editing = editing;
      }
    );

    this.employeeRemovedSubscription = this.employeesService.employeeRemoved
      .subscribe((employeeId: number) =>
      {
        console.log(this.department.departmentEmployees);
        const found = this.department.departmentEmployees.find(x => x.employeeID == employeeId);
        // better to re-fetch after remove
        console.log(found);
        if(found)
        {
          this.departmentsService.fetchDepartment(this.department.id);
        }
        else if(this.department.head && this.department.head.employeeID == employeeId)
        {
          this.department.head = null;
        }
      }
    );
  }

  ngOnDestroy(): void
  {
    this.currentDepartmentChangedSubscription.unsubscribe();
    this.editingChangedSubscription.unsubscribe();
  }

  startNewEmployee()
  {
    this.employeesService.startEditing()
  }
}
