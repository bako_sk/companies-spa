import { Injectable } from '@angular/core';
import {DepartmentModel} from '../models/department.model';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService
{

  private currentDepartment: DepartmentModel;
  public currentDepartmentChanged  = new Subject<DepartmentModel>();

  constructor(private http: HttpClient) { }

  public getCurrentDepartment(): DepartmentModel
  {
    return this.currentDepartment;
  }
  public setCurrentDepartment(department: DepartmentModel): void
  {
    this.currentDepartment = department;
    this.currentDepartmentChanged.next(department);
  }

  public fetchDepartment(id: number): void
  {
    this.http.get<DepartmentModel>(environment.apiUrl + 'departments/' + id)
      .pipe(
        map((department: DepartmentModel) =>
        {
          console.log(department);
          return {
            ...department,
            departmentEmployees: department.departmentEmployees ? department.departmentEmployees : [],
            head: department.head ? department.head : null
          }
        }),
        tap((department: DepartmentModel) =>
        {
          this.setCurrentDepartment(department);
        })
      )
      .subscribe();
  }
}
