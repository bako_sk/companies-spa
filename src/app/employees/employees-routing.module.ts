import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmployeeDetailComponent} from './employee-detail/employee-detail.component';

const employeesRoutes: Routes = [
  {path: 'employees/:id', component: EmployeeDetailComponent},
];
@NgModule({
  imports: [
    RouterModule.forChild(employeesRoutes),
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeesRoutingModule{}
