import {EmployeeModel} from '../../models/employee.model';
import {Pipe, PipeTransform} from '@angular/core';
import {DepartmentEmployeeModel} from '../../models/department-employee.model';

@Pipe({
  name: 'castDepartmentEmployee',
  pure: true
})
export class CastDepartmentEmployeePipe implements PipeTransform
{
  transform(value: EmployeeModel, ...args: any[]): DepartmentEmployeeModel
  {
    return <DepartmentEmployeeModel> value;
  }

}
