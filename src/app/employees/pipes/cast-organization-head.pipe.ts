import {Pipe, PipeTransform} from '@angular/core';
import {EmployeeModel} from '../../models/employee.model';
import {OrganizationHeadModel} from '../../models/organization-head.model';

@Pipe({
  name: 'castOrganizationHead',
  pure: true
})
export class  CastOrganizationHeadPipe implements PipeTransform
{
  transform(value: EmployeeModel, ...args: any[]): OrganizationHeadModel
  {
    return <OrganizationHeadModel> value;
  }

}
