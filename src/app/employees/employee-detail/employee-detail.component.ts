import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmployeeModel} from '../../models/employee.model';
import {Subscription} from 'rxjs';
import {EmployeesService} from '../employees.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit, OnDestroy {

  public employee: EmployeeModel;
  private currentEmployeeChangedSubscription: Subscription;
  private currentEmployeeRemovedSubscription: Subscription;

  public editing: boolean;
  private editingChangedSubscription: Subscription;
  constructor(
    public employeesService: EmployeesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.editing = false;
    this.employee = this.employeesService.getCurrentEmployee();
    this.activatedRoute.params.subscribe((params: Params) =>
    {
      this.employeesService.fetchEmployee(+params['id']);
    });
    this.currentEmployeeChangedSubscription = this.employeesService.currentEmployeeChanged
      .subscribe((employee: EmployeeModel) =>
      {
        this.employee = employee;
      }
    );

    this.editingChangedSubscription = this.employeesService.editingChanged
      .subscribe((editing: boolean) =>
      {
         this.editing = editing;
      }
    );

    this.currentEmployeeRemovedSubscription = this.employeesService.employeeRemoved
      .subscribe((employeId: number) =>
      {
        this.router.navigate(['/companies']);
      }
    );
  }

  public ngOnDestroy(): void
  {
    this.currentEmployeeChangedSubscription.unsubscribe();
    this.editingChangedSubscription.unsubscribe();
    this.currentEmployeeRemovedSubscription.unsubscribe();
  }

  public onStartEditing()
  {
    this.employeesService.startEditing();
  }

  public removeEmployee()
  {
    this.employeesService.removeEmployee(this.employee.employeeID);
  }
}
