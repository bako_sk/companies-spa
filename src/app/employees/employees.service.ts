import {Injectable} from '@angular/core';
import {EmployeeModel} from '../models/employee.model';
import {EMPTY, Subject} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {AddEmployeeActions} from './add-employee-actions';
import {DepartmentsService} from '../departments/departments.service';
import {CompaniesService} from '../companies/companies.service';
import {ProjectsService} from '../projects/projects.service';
import {DivisionsService} from '../divisions/divisions.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  public readonly DIVISION_HEAD = 'DIVISION_HEAD';
  public readonly PROJECT_HEAD = 'PROJECT_HEAD';
  public readonly DEPARTMENT_HEAD = 'DEPARTMENT_HEAD';
  public readonly CEO = 'CEO';
  public readonly DEPARTMENT_EMPLOYEE = 'DEPARTMENT_EMPLOYEE';
  private currentEmployee: EmployeeModel;
  public currentEmployeeChanged = new Subject<EmployeeModel>();
  public editingChanged = new Subject<boolean>();
  // currently api doesnt return removed element so return just its ID
  //public employeeRemoved = new Subject<EmployeeModel>();
  public employeeRemoved = new Subject<number>();
  public editErrorMsg = new Subject<string>();
  constructor(
    private http: HttpClient,
    private depService: DepartmentsService,
    private companiesService: CompaniesService,
    private projectsService: ProjectsService,
    private divisionsService: DivisionsService
  ) { }

  public getCurrentEmployee(): EmployeeModel
  {
    return this.currentEmployee;
  }

  public setCurrentEmployee(employee: EmployeeModel): void
  {
    this.currentEmployee = employee;
    this.currentEmployeeChanged.next(employee);
  }

  public fetchEmployee(id: number): void
  {
    this.http.get(environment.apiUrl + 'employees/' + id)
      .pipe(
        map((employee: EmployeeModel) =>
        {
          // map to types??
          console.log(employee);
          return employee;
        }),
        tap((employee: EmployeeModel) =>
        {
          this.setCurrentEmployee(employee);
        })
      )
      .subscribe();
  }
  public removeEmployee(employeeId: number) : void
  {
    this.http.delete<boolean>(environment.apiUrl + 'employees/' + employeeId)
      .pipe(
        map((removed: boolean) =>
        {
          console.log(removed);
          // map to types??
          if(removed)
          {
            this.employeeRemoved.next(employeeId);
          }
          else
          {
            // report error
          }
        })
      )
      .subscribe();
  }
  public startEditing(): void
  {
    this.editingChanged.next(true);
  }
  public stopEditing(): void
  {
    this.editingChanged.next(false);
  }

  public updateEmployee(updatedEmployee: EmployeeModel)
  {
    console.log('update');
    let employeeDto = {...updatedEmployee, roleName: updatedEmployee.role.name};
    const updatedEmployeeDto = {
      employeeID: updatedEmployee.employeeID,
      employeeDto: employeeDto
    };
    this.http.put(environment.apiUrl + 'employees/' + updatedEmployee.employeeID, updatedEmployeeDto)
      .pipe(
        tap((updated: any) => {
          console.log(updated);
          this.fetchEmployee(updatedEmployee.employeeID)
            //this.setCurrentEmployee(updatedEmployee);
          this.stopEditing();
          }
        ),
        catchError((err) =>
        {
          return this.handleError(err);
        })
      )
      .subscribe();
  }

  public addEmployee(newEmployee: any, unitId: number = 1, addType: AddEmployeeActions = AddEmployeeActions.ADD_DEPARTMENT_EMPLOYEE)
  {
    const url = this.getAddUrl(addType);
    const organizationEmployee = {
      unitId: unitId,
      employee: {
        ...newEmployee,
        roleName: this.getRoleName(addType)
      }
    };
    this.http.post(url, organizationEmployee)
      .pipe(
        tap((updated: any) => {
          this.stopEditing();
            this.reloadAfterInsert(addType, unitId);
          }
        ),
        catchError((err) =>
        {
          return this.handleError(err);
        })
      )
      .subscribe();
  }

  private getAddUrl(addType: AddEmployeeActions)
  {
    let url = environment.apiUrl;
    switch (addType) {
      case AddEmployeeActions.ADD_COMPANY_CEO:
      {
        url += 'companies/add-ceo';
        break;
      }
      case AddEmployeeActions.ADD_DIVISION_HEAD:
      {
        url += 'divisions/add-head';
        break;
      }
      case AddEmployeeActions.ADD_PROJECT_HEAD:
      {
        url += 'projects/add-head';
        break;
      }
      case AddEmployeeActions.ADD_DEPARTMENT_HEAD:
      {
        url += 'departments/add-head';
        break;
      }
      case AddEmployeeActions.ADD_DEPARTMENT_EMPLOYEE:
      default:
      {
        url += 'departments/add-employee';
      }
    }
    return url;
  }

  private getRoleName(addType: AddEmployeeActions)
  {
    let roleName = '';
    switch (addType) {
      case AddEmployeeActions.ADD_DIVISION_HEAD:
      {
        roleName = this.DIVISION_HEAD;
        break;
      }
      case AddEmployeeActions.ADD_PROJECT_HEAD:
      {
        roleName = this.PROJECT_HEAD;
        break;
      }
      case AddEmployeeActions.ADD_DEPARTMENT_HEAD:
      {
        roleName = this.DEPARTMENT_HEAD;
        break;
      }
      case AddEmployeeActions.ADD_COMPANY_CEO:
      {
        roleName = this.CEO;
        break;
      }

      case AddEmployeeActions.ADD_DEPARTMENT_EMPLOYEE:
      default:
      {
        roleName = this.DEPARTMENT_EMPLOYEE;
      }
    }
    return roleName;
  }

  private reloadAfterInsert(addType: AddEmployeeActions, unitId: number)
  {
    switch (addType) {
      case AddEmployeeActions.ADD_DIVISION_HEAD:
      {
        this.divisionsService.fetchDivision(unitId);
        break;
      }
      case AddEmployeeActions.ADD_PROJECT_HEAD:
      {
        this.projectsService.fetchProject(unitId);
        break;
      }
      case AddEmployeeActions.ADD_COMPANY_CEO:
      {
        this.companiesService.fetchCompany(unitId);
        break;
      }
      case AddEmployeeActions.ADD_DEPARTMENT_HEAD:
      case AddEmployeeActions.ADD_DEPARTMENT_EMPLOYEE:
      default:
      {
        this.depService.fetchDepartment(unitId);
      }
    }
  }
  private handleError(err: any)
  {

    let msg = '';
    if(err instanceof HttpErrorResponse && err.status == 422) {
      msg = 'Client sent wrong entity to server';
    }
    if(err instanceof HttpErrorResponse && err.status == 400) {
      if(err.error.errors.hasOwnProperty('Employee.Email'))
      {
            msg += err.error.errors['Employee.Email'];
      }
      else if(err.error.errors.hasOwnProperty('EmployeeDto.Email'))
      {
        msg += err.error.errors['EmployeeDto.Email'];
      }
      else
      {
        msg = "Client data didn't pass server validation";
      }
    }
    if(msg != '')
    {
      this.editErrorMsg.next(msg);
    }
    return EMPTY;
  }

}
