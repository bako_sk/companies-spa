import {Component, Input, OnInit} from '@angular/core';
import {EmployeeModel} from '../../models/employee.model';
import {EmployeesService} from '../employees.service';

@Component({
  selector: 'app-employee-item',
  templateUrl: './employee-item.component.html',
  styleUrls: ['./employee-item.component.css']
})
export class EmployeeItemComponent implements OnInit {

  @Input() employee: EmployeeModel;
  @Input() showRole?: boolean;
  constructor(
    private employeeService: EmployeesService
  ) { }

  ngOnInit() {
  }

  deleteEmployee()
  {
    this.employeeService.removeEmployee(this.employee.employeeID);
  }
}
