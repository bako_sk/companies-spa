import {Component, Input, OnInit } from '@angular/core';
import {EmployeeModel} from '../../models/employee.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployeesService} from '../employees.service';
import {AddEmployeeActions} from '../add-employee-actions';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit{

  @Input() editedEmployee?: EmployeeModel;
  @Input() unitId?: number;
  @Input() addType?: AddEmployeeActions;
  public empForm: FormGroup;
  public errorMsg: string;
  private editErrorMsgSubscription: Subscription;

  constructor(private employeesService: EmployeesService) { }

  public ngOnInit() {
    this.initForm();
    this.editErrorMsgSubscription = this.employeesService.editErrorMsg
      .subscribe((msg: string) =>
      {
        this.errorMsg = msg;
      });
  }

  public onSubmit()
  {
    this.errorMsg = '';
    //update
    const newEmployee = this.empForm.value;
    if(this.editedEmployee)
    {
      if(this.emplooyesAreEqual(this.editedEmployee,newEmployee))
      {
        this.errorMsg = 'Nothing changed';
        return;
      }
      newEmployee.employeeID = this.editedEmployee.employeeID;
      newEmployee.role = this.editedEmployee.role;
      this.employeesService.updateEmployee(newEmployee);
      console.log(newEmployee);
    }
    // add
    else
    {
      this.employeesService.addEmployee(newEmployee, this.unitId, this.addType);
    }
    // stop editing after update/add is done
    //this.employeesService.stopEditing();
  }

  public cancelEditing()
  {
    this.employeesService.stopEditing();
  }
  private initForm()
  {
    let degree = '';
    let firstName = '';
    let lastName = '';
    let phoneExt = '';
    let email = '';

    if (this.editedEmployee != null) {
      degree = this.editedEmployee.degree;
      firstName = this.editedEmployee.firstName;
      lastName = this.editedEmployee.lastName;
      phoneExt = this.editedEmployee.phoneExt;
      email = this.editedEmployee.email;
    }

    this.empForm = new FormGroup({
      degree: new FormControl(degree),
      firstName: new FormControl(firstName, Validators.required),
      lastName: new FormControl(lastName, Validators.required),
      phoneExt: new FormControl(phoneExt, [Validators.required, Validators.pattern(/^[0-9]+$/)]),
      email: new FormControl(email, [Validators.required, Validators.email]),
    });
  }
  // cannot be in the model, because it might get initilized from json, not as object(fix?)
  private emplooyesAreEqual(employee1: EmployeeModel, employee2: EmployeeModel): boolean
  {
    return employee1.firstName === employee2.firstName
        && employee1.lastName == employee2.lastName
        && employee1.degree === employee2.degree
        && employee1.email === employee2.email
        && employee1.phoneExt === employee2.phoneExt;
  }

}
