import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeItemComponent } from './employee-item/employee-item.component';
import {RouterModule} from '@angular/router';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import {SharedModule} from '../shared/shared.module';
import {EmployeesRoutingModule} from './employees-routing.module';
import {CastDepartmentEmployeePipe} from './pipes/cast-department-employee.pipe';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CastOrganizationHeadPipe} from './pipes/cast-organization-head.pipe';



@NgModule({
  declarations: [EmployeeItemComponent,
    EmployeeDetailComponent,
    CastDepartmentEmployeePipe,
    CastOrganizationHeadPipe,
    EmployeeEditComponent
  ],
  exports: [
    EmployeeItemComponent,
    EmployeeEditComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    RouterModule,
    SharedModule,
    EmployeesRoutingModule
  ]
})
export class EmployeesModule { }
