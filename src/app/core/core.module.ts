import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../app-routing.module';



@NgModule({
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent,
    AppRoutingModule
  ],
  imports: [
    BrowserModule,
    // must be imported for routing to work for header?maybe move header to app n ot core?
    AppRoutingModule
  ]
})
export class CoreModule { }
