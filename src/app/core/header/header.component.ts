import {Component, OnDestroy, OnInit} from '@angular/core';
import {CompaniesService} from '../../companies/companies.service';
import {CompanyModel} from '../../models/company.model';
import {Subscription} from 'rxjs';
import {DivisionModel} from '../../models/division.model';
import {DivisionsService} from '../../divisions/divisions.service';
import {ProjectModel} from '../../models/project-model';
import {DepartmentModel} from '../../models/department.model';
import {ProjectsService} from '../../projects/projects.service';
import {DepartmentsService} from '../../departments/departments.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public currentCompany: CompanyModel;
  private currentCompanyChangedSubscription: Subscription;

  public currentDivision: DivisionModel;
  private currentDivisionChangedSubscription: Subscription;

  public currentProject: ProjectModel;
  private currentProjectChangedSubscription: Subscription;

  public currentDepartment: DepartmentModel;
  private currentDepartmentChangedSubscription: Subscription;

  constructor(
    private companiesService: CompaniesService,
    private divisionsService: DivisionsService,
    private projectsService: ProjectsService,
    private departmentsService: DepartmentsService)
  {
    this.currentCompany = null;
    this.currentDivision = null;
    this.currentProject = null;
    this.currentDepartment = null;
  }

  ngOnInit()
  {
    this.currentCompany = this.companiesService.getCurrentCompany();
    this.currentCompanyChangedSubscription = this.companiesService.currentCompanyChanged
      .subscribe((company: CompanyModel) =>
      {
        this.currentCompany = company;
      }
    );

    this.currentDivision = this.divisionsService.getCurrentDivison();
    this.currentDivisionChangedSubscription = this.divisionsService.currentDivisionChanged
      .subscribe((division: DivisionModel) =>
      {
        this.currentDivision = division;
      }
    );

    this.currentProject = this.projectsService.getCurrentProject();
    this.currentProjectChangedSubscription = this.projectsService.currentProjectChanged
      .subscribe((project: ProjectModel) =>
      {
        this.currentProject = project;
      }
    );

    this.currentDepartment = this.departmentsService.getCurrentDepartment();
    this.currentDepartmentChangedSubscription = this.departmentsService.currentDepartmentChanged
      .subscribe((department: DepartmentModel) =>
      {
        this.currentDepartment = department;
      }
    );
  }

  ngOnDestroy(): void
  {
    this.currentCompanyChangedSubscription.unsubscribe();
    this.currentDivisionChangedSubscription.unsubscribe();
    this.currentProjectChangedSubscription.unsubscribe();
    this.currentDepartmentChangedSubscription.unsubscribe();
  }

}
