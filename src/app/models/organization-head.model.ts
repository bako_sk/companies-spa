import {EmployeeModel} from './employee.model';
import {EmployeeRoleModel} from './employee-role.model';
import {OrganizationUnitModel} from './organizationUnit.model';

export class OrganizationHeadModel extends EmployeeModel
{

  constructor(
    public employeId: number,
    public degree: string,
    public firstName: string,
    public lastName: string,
    public phoneExt: string,
    public email: string,
    public role: EmployeeRoleModel,
    public headOf: OrganizationUnitModel)
  {
    super(employeId, degree, firstName, lastName, phoneExt, email, role);
    this.headOf = headOf;
  }
}
