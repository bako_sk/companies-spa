import {EmployeeModel} from './employee.model';
import {EmployeeRoleModel} from './employee-role.model';
import {DepartmentModel} from './department.model';

export class DepartmentEmployeeModel extends EmployeeModel
{

  constructor(
    public employeId: number,
    public degree: string,
    public firstName: string,
    public lastName: string,
    public phoneExt: string,
    public email: string,
    public role: EmployeeRoleModel,
    public departments: DepartmentModel[])
  {
    super(employeId, degree, firstName, lastName, phoneExt, email, role);
    this.departments = departments;
  }
}
