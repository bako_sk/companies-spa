import {OrganizationUnitModel} from './organizationUnit.model';
import {EmployeeModel} from './employee.model';
import {DivisionModel} from './division.model';

export class CompanyModel extends OrganizationUnitModel {


  constructor(
    public id: number,
    public name: string,
    public code: string,
    public head: EmployeeModel,
    public divisions: DivisionModel[]
  ) {
      super(id, name, code, head);
      this.divisions = divisions;
  }
}
