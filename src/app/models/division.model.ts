import {OrganizationUnitModel} from './organizationUnit.model';
import {EmployeeModel} from './employee.model';
import {ProjectModel} from './project-model';
import {CompanyModel} from './company.model';

export class DivisionModel extends OrganizationUnitModel
{

  constructor(
    public id: number,
    public name: string,
    public code: string,
    public head: EmployeeModel,
    public projects: ProjectModel[],
    public company: CompanyModel)
  {
    super(id, name, code, head);
    this.projects = projects;
  }
}
