import {EmployeeModel} from './employee.model';

export abstract class OrganizationUnitModel {

  public constructor(public id: number, public name: string, public code: string, public head: EmployeeModel) {}
}
