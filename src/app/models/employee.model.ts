import {EmployeeRoleModel} from './employee-role.model';

export class EmployeeModel {
  public constructor(
    public employeeID: number,
    public degree: string,
    public firstName: string,
    public lastName: string,
    public phoneExt: string,
    public email: string,
    public role: EmployeeRoleModel
    ) {}


}
