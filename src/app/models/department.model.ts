import {OrganizationUnitModel} from './organizationUnit.model';
import {EmployeeModel} from './employee.model';
import {ProjectModel} from './project-model';

export class DepartmentModel extends OrganizationUnitModel{


  constructor(
    public id: number,
    public name: string,
    public code: string,
    public head: EmployeeModel,
    public departmentEmployees: EmployeeModel[],
    public project: ProjectModel)
  {
    super(id, name, code, head);
  }
}
