import {OrganizationUnitModel} from './organizationUnit.model';
import {EmployeeModel} from './employee.model';
import {DepartmentModel} from './department.model';
import {DivisionModel} from './division.model';

export class ProjectModel extends OrganizationUnitModel{


  constructor(
    public id: number,
    public name: string,
    public code: string,
    public head: EmployeeModel,
    public departments: DepartmentModel[],
    public division: DivisionModel)
  {
    super(id, name, code, head);
    this.departments = departments;
  }
}
