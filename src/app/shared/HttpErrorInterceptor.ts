import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {EMPTY, Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor{

  constructor(private router: Router) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
  {
    return next.handle(req)
      .pipe(
        catchError((err, caught) => {
          if (!environment.production) {
            console.log(err);
          }
          if(err instanceof HttpErrorResponse) {
            if (err.status == 404) {
              return this.handleErr('/not-found');
            }
            else if (err.status == 500) {
              return  this.handleErr('/server-error');
            }
          }
          return throwError(err);
        })
      );
  }

  private handleErr(redirectTo: string)
  {
    this.router.navigate([redirectTo], {replaceUrl: true});

    //?? do not throw error, just empty observable;
    return EMPTY;
  }


}
