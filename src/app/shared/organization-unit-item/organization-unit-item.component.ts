import {Component, Input, OnInit} from '@angular/core';
import {OrganizationUnitModel} from '../../models/organizationUnit.model';

@Component({
  selector: 'app-organization-unit-item',
  templateUrl: './organization-unit-item.component.html',
  styleUrls: ['./organization-unit-item.component.css']
})
export class OrganizationUnitItemComponent  {

  @Input() unit: OrganizationUnitModel;
  @Input() unitType: string;
  constructor() { }

  public getRouterLink(): string
  {
    return '/' + this.unitType + '/' + this.unit.id;
  }


}
