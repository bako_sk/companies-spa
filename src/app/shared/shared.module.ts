import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrganizationUnitItemComponent} from './organization-unit-item/organization-unit-item.component';
import {RouterModule} from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { ServerErrorComponent } from './server-error/server-error.component';



@NgModule({
  declarations: [OrganizationUnitItemComponent, NotFoundComponent, ServerErrorComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    OrganizationUnitItemComponent
  ]
})
export class SharedModule { }
