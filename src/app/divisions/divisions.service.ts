import { Injectable } from '@angular/core';
import {DivisionModel} from '../models/division.model';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DivisionsService {

  private  currentDivision: DivisionModel;
  public currentDivisionChanged = new Subject<DivisionModel>();

  constructor(private http: HttpClient) { }

  public getCurrentDivison(): DivisionModel
  {
    return this.currentDivision;
  }

  public setCurrentDivision(division: DivisionModel)
  {
    this.currentDivision = division;
    this.currentDivisionChanged.next(division);
  }

  public fetchDivision(id: number)
  {
    this.http.get<DivisionModel>(environment.apiUrl + 'divisions/' + id)
      .pipe(
        map((division: DivisionModel) =>
        {
          console.log(division);
          // immutable
          return {
            ...division,
            projects: division.projects ? division.projects : [],
            head: division.head ? division.head : null
          }
        }),
        tap((division: DivisionModel) =>
        {
          this.setCurrentDivision(division)
        })
      )
      .subscribe();
  }
}
