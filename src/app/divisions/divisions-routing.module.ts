import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DivisionDetailComponent} from './division-detail/division-detail.component';

const divisionRoutes: Routes = [
  {path: 'divisions/:id', component: DivisionDetailComponent}
];
@NgModule({
  imports: [
    RouterModule.forChild(divisionRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class DivisionsRoutingModule {}
