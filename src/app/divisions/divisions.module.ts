import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DivisionDetailComponent } from './division-detail/division-detail.component';
import {DivisionsRoutingModule} from './divisions-routing.module';
import {SharedModule} from '../shared/shared.module';
import {EmployeesModule} from '../employees/employees.module';



@NgModule({
  declarations: [DivisionDetailComponent],
  imports: [
    CommonModule,
    DivisionsRoutingModule,
    SharedModule,
    EmployeesModule
  ]
})
export class DivisionsModule { }
