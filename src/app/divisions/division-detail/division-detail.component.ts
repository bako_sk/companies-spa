import {Component, OnDestroy, OnInit} from '@angular/core';
import {DivisionModel} from '../../models/division.model';
import {DivisionsService} from '../divisions.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs';
import {EmployeesService} from '../../employees/employees.service';
import {AddEmployeeActions} from '../../employees/add-employee-actions';

@Component({
  selector: 'app-division-detail',
  templateUrl: './division-detail.component.html',
  styleUrls: ['./division-detail.component.css']
})
export class DivisionDetailComponent implements OnInit, OnDestroy {

  public addType = AddEmployeeActions.ADD_DIVISION_HEAD;
  public division: DivisionModel;

  private currentDivisionChangedSubscription: Subscription;
  private employeeRemovedSubscription: Subscription;

  constructor(
    private divisionsService: DivisionsService,
    private employeesService: EmployeesService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit()
  {
    this.division = this.divisionsService.getCurrentDivison();
    this.activatedRoute.params.subscribe((params: Params) =>
    {
      this.divisionsService.fetchDivision(+params['id']);
    });

    this.currentDivisionChangedSubscription = this.divisionsService.currentDivisionChanged
      .subscribe((division: DivisionModel) =>
      {
        this.division = division;
      }
    );

    this.employeeRemovedSubscription = this.employeesService.employeeRemoved
      .subscribe((removedId: number) =>
      {
        if(this.division && this.division.head.employeeID == removedId)
        {
          this.division.head = null;
        }
      });
  }

  ngOnDestroy(): void
  {
    this.currentDivisionChangedSubscription.unsubscribe();
    this.employeeRemovedSubscription.unsubscribe();
  }

}
