import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompaniesListComponent } from './companies-list/companies-list.component';
import {CompaniesRoutingModule} from './companies-routing.module';
import { CompaniesComponent } from './companies/companies.component';
import { CompanyDetailComponent } from './company-detail/company-detail.component';
import {SharedModule} from '../shared/shared.module';
import {EmployeesModule} from '../employees/employees.module';



@NgModule({
  declarations: [CompaniesListComponent, CompaniesComponent, CompanyDetailComponent],
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    SharedModule,
    EmployeesModule
  ]
})
export class CompaniesModule { }
