import {Component, OnDestroy, OnInit} from '@angular/core';
import {CompaniesService} from '../companies.service';
import {CompanyModel} from '../../models/company.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-companies-list',
  templateUrl: './companies-list.component.html',
  styleUrls: ['./companies-list.component.css']
})
export class CompaniesListComponent implements OnInit, OnDestroy {

  public companies: CompanyModel[];
  private companiesChangedSubscription: Subscription;

  constructor(private companiesService: CompaniesService) { }

  ngOnInit()
  {
    this.companies = this.companiesService.getCompanies();
    this.companiesService.fetchCompanies();

    this.companiesChangedSubscription = this.companiesService.companiesChanged
      .subscribe((companies: CompanyModel[]) =>
      {
        this.companies = companies;
      });
  }

  ngOnDestroy(): void
  {
    this.companiesChangedSubscription.unsubscribe();
  }

}
