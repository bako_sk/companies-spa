import {Component, OnDestroy, OnInit} from '@angular/core';
import {CompanyModel} from '../../models/company.model';
import {ActivatedRoute, Params} from '@angular/router';
import {CompaniesService} from '../companies.service';
import {Subscription} from 'rxjs';
import {AddEmployeeActions} from '../../employees/add-employee-actions';
import {EmployeesService} from '../../employees/employees.service';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.css']
})
export class CompanyDetailComponent implements OnInit, OnDestroy {
  public addType = AddEmployeeActions.ADD_COMPANY_CEO;
  public company: CompanyModel;

  private currentCompanySubscription: Subscription;
  private employeeRemovedSubscription: Subscription;

  constructor(
    private companiesService: CompaniesService,
    private employeesService: EmployeesService,
    private route: ActivatedRoute
  ) { }

  ngOnInit()
  {
    this.company = this.companiesService.getCurrentCompany();
    this.route.params
      .subscribe(
        (params: Params) =>
        {
          this.companiesService.fetchCompany(+params['id']);
        }
      );
    this.currentCompanySubscription = this.companiesService.currentCompanyChanged
      .subscribe((company: CompanyModel) =>
      {
        this.company = company;
      }
    );
    this.employeeRemovedSubscription = this.employeesService.employeeRemoved
      .subscribe(
        (removedId: number) =>
        {
          if(this.company && this.company.head.employeeID == removedId)
          {
            this.company.head = null;
          }
        }
      );
  }

  ngOnDestroy(): void
  {
    this.currentCompanySubscription.unsubscribe();
    this.employeeRemovedSubscription.unsubscribe();
  }

}
