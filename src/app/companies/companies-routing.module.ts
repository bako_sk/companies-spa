import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CompaniesComponent} from './companies/companies.component';
import {CompanyDetailComponent} from './company-detail/company-detail.component';

const companiesRoutes: Routes = [
  {path: 'companies', component: CompaniesComponent},
  // if want to use as a children of companies component, it would have to have router outet
  //  children:
  // [
  //  {path: ':id', component: CompanyDetailComponent}
  // ]},
  // this way detail comoponet displayed in router outlet of root component(app-component)
  {path: 'companies/:id', component: CompanyDetailComponent}
];
@NgModule({
  imports: [
    RouterModule.forChild(companiesRoutes),
  ],
  exports: [
    RouterModule
  ]
  })
export class CompaniesRoutingModule {}
