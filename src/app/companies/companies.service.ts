import { Injectable } from '@angular/core';
import {CompanyModel} from '../models/company.model';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  public companiesChanged = new Subject<CompanyModel[]>();
  private companies: CompanyModel[];
  public  currentCompanyChanged = new Subject<CompanyModel>();
  private currentCompany: CompanyModel;

  constructor(private http: HttpClient)
  {
    this.currentCompany = null;
    this.companies = [];
  }

  public getCompanies(): CompanyModel[]
  {
      return this.companies;
  }

  public setCompanies(companies: CompanyModel[])
  {
    this.companies = companies;
    this.companiesChanged.next(this.companies.slice());
  }
  public getCurrentCompany(): CompanyModel
  {
    return this.currentCompany;
  }
  setCurrentCompany(company: CompanyModel)
  {
    this.currentCompany = company;
    this.currentCompanyChanged.next(this.currentCompany);

  }

  public fetchCompanies()
  {
    return this.http.get<CompanyModel[]>(environment.apiUrl + 'companies')
      .pipe(
        map((companies: CompanyModel[]) => {
          console.log(companies);
          return companies.map((company: CompanyModel) => {
            return {
              ...company,
              // in case divisions are not returned by api, set to [] (for example for list of companies)
              divisions: company.divisions ? company.divisions : [],
              ceo: company.head ? company.head : [],
            };
          });
        }),
        tap((companies: CompanyModel[]) => {
          this.setCompanies(companies);
        })
      )
      // must subscribe to send the request
      .subscribe();
  }
  public fetchCompany(id: number)
  {
    return this.http.get<CompanyModel>(environment.apiUrl + 'companies/' + id)
      .pipe(
        map((company: CompanyModel) => {
          console.log(company);
          return {
            ...company,
            // in case divisions are not returned by api, set to [] (for example for list of companies)
            divisions: company.divisions ? company.divisions : [],
            head: company.head ? company.head: null,
          };
        }),
        tap((company: CompanyModel) => {
          this.setCurrentCompany(company);
        })
      )
      // must subscribe to send the request
      .subscribe();
  }
}
